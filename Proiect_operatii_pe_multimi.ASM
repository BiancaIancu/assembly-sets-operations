.386
.model flat, stdcall

includelib msvcrt.lib
extern exit:proc
extern scanf:proc
extern printf:proc
extern fprintf: proc
extern fopen: proc
extern fclose: proc

public start


.data
	spatiu db " ",13,10,13,10,0
	operatie1 DB "1.Verificare",13,10,0
	operatie2 DB "2.Apartenenta",13,10,0
	operatie3 DB "3.Reuniune",13,10,0
	operatie4 DB "4.Intersectie",13,10,0
	
	selectie db 0 
	a dd 50 dup(?)
	b dd 50 dup(?)
	d dd 50 dup(?)
	aux dd 0
	nra dd 0
	nrb dd 0
	nrd dd 0
	msg1 db "Introduceti nr elemente ale multimii a: ", 0
	msg2 db 'Introduceti nr elemente ale multimii b: ', 0
	msg3 db 'Introduceti elementele multimii a:', 0
	msg4 db 'Introduceti elementele multimii b:', 0
	msg5 db 'Multime incorecta.', 0
	msg6 db 'Multime corecta',0
	format db "%d", 0
	msg db ' ', 0

	text4 db "Dati un element: ",0
	el dd 0 
	format_el db "%d",0
	text_el db "Elementul tau este %d",13,10,0
	text5 db "Elementul se gaseste in multime", 13,10,0
	text6 db "Elementul nu se gaseste in multime", 13,10,0
	j dd 0
	
	mode_w db "w", 0
	file_name db "Rezultat.txt",0

buffer dd 0

.code

meniu MACRO
		local afis_meniu
		afis_meniu:
		;;;;AFISARE MENIU;;;;;;;;;;
		push offset spatiu 
		call printf
		add esp,4 
		
		push offset operatie1
		call printf
		add esp,4
		
		push offset operatie2
		call printf
		add esp,4 
		
		push offset operatie3
		call printf
		add esp,4
		
		push offset operatie4
		call printf
		add esp,4
		
ENDM

selectare macro
			push offset selectie
			push offset format
			call scanf
			add esp,8
			
			mov al,selectie
			
			cmp al,1
			je caz1
		
			cmp al,2
			je caz2
		
			cmp al,3
			je caz3
		
			cmp al,4
			je caz4
			
			jmp start
		
			caz1:
			verificare
			jmp start
			caz2:apartenenta
			jmp start
			caz3:
			citire_a
			citire_b
			reuniunea
			jmp start
			caz4:
			citire_a
			citire_b
			
			intersectia
			jmp start
			
endm



	
citire_a macro
	local citirea
	mov nrd, 0
	;afisare mesaj citire nr elemente a
	push offset msg1
	call printf
	add esp, 4
	
	;citire numar elemente ale multimii a
	push offset buffer
	push offset format
	call scanf
	add esp, 8
	mov eax, buffer
	mov nra, eax
	
	;citire elemente a
	push offset msg3
	call printf
	add esp, 4
	
	mov esi, 0
	citirea:
		push offset buffer
		push offset format
		call scanf
		add esp, 8
		mov eax, buffer
		mov a[esi*4], eax
		inc esi
		cmp esi, nra
	jl citirea
endm

citire_b macro	
	local citireb
	;afisare mesaj pt citire nr elemente din b
	push offset msg2
	call printf
	add esp, 4
	
	;citire numar elemente ale multimii a
	push offset buffer
	push offset format
	call scanf
	add esp, 8
	mov eax, buffer
	mov nrb, eax
	
	;afisare mesaj pt citire elemente ale lui b
	push offset msg4
	call printf
	add esp, 4
	
	;citire elemente ale lui b
	mov esi, 0
	citireb:
		push offset buffer
		push offset format
		call scanf
		add esp, 8
		mov eax, buffer
		mov b[esi*4], eax
		inc esi
		cmp esi, nrb
	jl citireb

endm




verificare macro
	;verificare 
	local verif,gresit
	citire_a
	
	
	mov esi, 0
	mov edi, 1
	verif:
	mov eax, a[esi*4]
	cmp eax, a[edi*4]
	je gresit
	inc edi ;daca nu s egale, trec la urmatorul element din edi
	cmp edi, nra    ;daca edi inca nu a ajuns la final, inca verific
	jle verif
	inc esi  ;cand edi ajunge la final
	mov eax,esi
	inc eax
	mov edi,eax
	cmp esi,nra
	jl verif         
	jmp corect
	
	corect:
	push offset msg6
	call printf
	add esp,4
	jmp start
	
	gresit:
	push offset msg5
	call printf
	add esp, 4
	jmp start
	
	
	
endm

apartenenta macro
		local parcurgere_vector, apartine, nu_apartine
		
		push offset text4
		call printf
		add esp,4
		
		push offset el
		push offset format_el
		call scanf
		add esp,8
		
		
		
		citire_a 
		
		lea esi, a
			mov j,0
			parcurgere_vector:
			mov eax, [esi]
			cmp eax, el
			je apartine
			
			inc j
			add esi,4
			mov eax, nra
			cmp j,eax
			jl parcurgere_vector
			
			cmp j,eax
			jle nu_apartine
			
			nu_apartine:
			push offset text6
			call printf
			add esp,4
			jmp start 
			
	
			
			apartine:
			push offset text5
			call printf
			add esp,4
			
endm

reuniunea macro
	
	mov eax,nrb
	mov nrd, eax
	mov esi,0
	mov edi,0
	mov ebp,0
	mov ebp, nrd
	
	reuniune:
	mov eax,a[esi*4]
	cmp eax,b[edi*4]
	je continua  ;daca gasim elementul din a in b atunci nu mai are rost sa cautam
					;asa ca vom continua la urmatorul element din a
	inc edi			;dar daca nu il gasim, continuam sa cautam in b
	cmp edi,nrb
	jl reuniune ;daca nu am ajuns la finalul lui b atunci continui sa compar
	mov b[ebp*4],eax
	inc ebp
	inc esi;daca am ajuns la final il readucem pe b la prima pozitie si pe a la urm elemeny
	mov edi,0
	cmp esi,nra
	jl reuniune
	jmp stop2
	
	continua:  	
				inc esi
				mov edi,0
				cmp esi,nra
				jl reuniune
				mov b[ebp*4],eax
				inc ebp
	
	stop2:
	
	mov nrd,ebp
	mov esi, 0
	afisare:
	push b[esi*4]
	push offset format
	call printf
	add esp, 8
	push offset msg
	call printf
	add esp, 4
	inc esi
	cmp esi, nrd
	jl afisare
endm

intersectia macro
	
	local intersectie, adauga,stop,afisare

	
	mov esi, 0
	mov edi, 0
	
	intersectie:
	mov eax, a[esi*4]   
	cmp eax, b[edi*4]
	
	je adauga ;daca el sunt egale, le pun in vectorul d
	inc edi  ; daca nu sunt egale, ma duc la elementul urmator din b
	cmp edi, nra    ; verific daca nu am ajuns la final cu b
	jl intersectie            ;daca nu am ajuns la final continui sa compar
	inc esi                 ;daca am ajuns la final, il readuc pe d pe prima pozitie, si pe a la urmatorul element
	mov edi, 0
	cmp esi, nrb                   
	jl intersectie
	jmp stop
	
	adauga:
	mov ebx, nrd      ;initial nrd e 0
	mov d[ebx*4], eax   ;in eax se afla intersectia
	inc nrd           ;creste nr de elemente din d
	inc esi            ; ma duc la urmatorul el din vectorul a
	mov edi, 0         ;pe acesta il compar din nou cu toate elementele din b, deci in b trebuie sa incep cu primul element
	cmp esi, nra              ;verific daca am ajuns la sfarsitul vectorului a
	jl intersectie    	;daca nu am ajuns, incep compararea
	
	stop:
	
	mov esi, 0
	afisare:
	push d[esi*4]
	push offset format
	call printf
	add esp, 8
	push offset msg
	call printf
	add esp, 4
	inc esi
	cmp esi, nrd
	jl afisare

endm



start:
	meniu
	selectare

	
	push 0
	call exit
end start

